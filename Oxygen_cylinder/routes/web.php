<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\PreventBackHistory;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([PreventBackHistory::class])->group(function () {
    Route::resource('users', 'NormalUsersController');
   
    //Route::resource('oxygen_cylinders','SuppliersController');
    Route::get('login', 'AuthController@index')->name('login');
    Route::post('post-login', 'AuthController@postLogin')->name('post-login');
    Route::get('register', 'AuthController@register')->name('register');
    Route::post('post-register', 'AuthController@postRegister')->name('post-register');
    Route::get('logout', 'AuthController@logout')->name('logout');
    Route::get('getEmployees', 'NormalUsersController@getEmployees')->name('getEmployees');
    Route::post('dropzone/store', 'DropzoneController@dropzoneStore')->name('dropzone.store');
});
Route::post('create_users', 'NormalUsersController@store')->name('create_users');