<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier_Users extends Model
{
    protected $fillable = [
        'supplier_name','supplier_gender','supplier_age','supplier_aadhar_card_number','supplier_identity_proof','supplier_address','supplier_state','supplier_city','supplier_phone_number','supplier_password','supplier_email','oxy1','oxy2','oxy3'
    ];
}
