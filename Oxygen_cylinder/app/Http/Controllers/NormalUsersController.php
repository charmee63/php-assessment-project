<?php

namespace App\Http\Controllers;

use App\Oxygen_Cylinders;
use Illuminate\Http\Request;
use App\User;

class NormalUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __contruct(){
      $this->middleware('auth');
      //$this->middleware(PreventBackHistory::class);
    }
    public function index()
    {
        return view('users.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    public function getEmployees(Request $request){

        ## Read value
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page
   
        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');
   
        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value
   
        // Total records
        $totalRecords = Oxygen_Cylinders::select('count(*) as allcount')->count();
        $totalRecordswithFilter = Oxygen_Cylinders::select('count(*) as allcount')->where('users_name', 'like', '%' .$searchValue . '%')->count();
   
        // Fetch records
        $records = Oxygen_Cylinders::select('*')->where('users_name', 'like', '%' .$searchValue . '%')->orderBy($columnName,$columnSortOrder)
          ->skip($start)
          ->take($rowperpage)
          ->get();

        $array_data=json_decode(json_encode($records), true);
       
   
          $data_arr =[];
        
        foreach($array_data as $record){ 
            $delete = "delete_user_data('".base64_encode($record['id'])."');";
            $actions = "<a class='btn btn-info' target='_parent' href='".route('users.show',$record['id'])."'> Show</a><a class='btn btn-primary' target='_parent' href='".route('users.edit',$record['id'])."'> Edit</a>";
          
           $data_arr[] = array(
             "id" =>  $record['id'],
             "username" => $record['users_name'],
             "name" => $record['users_state'],
             "email" => $record['users_city'],
             "actions"=>$actions,
           );
        }

        
      
        $response = array(
           "draw" => intval($draw),
           "iTotalRecords" => $totalRecords,
           "iTotalDisplayRecords" => $totalRecordswithFilter,
           "aaData" => $data_arr
        );
   
        echo json_encode($response);
        exit;
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $data = $request->all();
 
        $check = $this->create_cylinder_data($data);
        if($check){
          echo json_encode(array("status"=>"success"));
        }else{
          echo json_encode(array("status"=>"failure"));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Oxygen_Cylinders  $oxygen_Cylinders
     * @return \Illuminate\Http\Response
     */
    public function show(Oxygen_Cylinders $oxygen_Cylinders)
    {
        $id=explode("/",url()->current());
        $id_value=$id[6];
        $oxygen_cylinders = Oxygen_Cylinders::where('id', $id_value)->first();
        return view('users.show',compact('oxygen_cylinders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Oxygen_Cylinders  $oxygen_Cylinders
     * @return \Illuminate\Http\Response
     */
    public function edit(Oxygen_Cylinders $oxygen_Cylinders)
    {
        $id=explode("/",url()->current());
        $id_value=$id[6];
        $oxygen_cylinders = Oxygen_Cylinders::where('id', $id_value)->first();
        return view('users.edit',compact('oxygen_cylinders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Oxygen_Cylinders  $oxygen_Cylinders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Oxygen_Cylinders $oxygen_Cylinders)
    {
        $id=explode("/",url()->current());
        $id_value=$id[6];
        $request->validate([
            'users_booked_cylinder_status' => 'required',
        ]);

        $subject = Oxygen_Cylinders::find($id_value);
  
        $subject->update($request->all());
  
        return redirect()->route('users.index')->with('success','Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Oxygen_Cylinders  $oxygen_Cylinders
     * @return \Illuminate\Http\Response
     */
    public function destroy(Oxygen_Cylinders $oxygen_Cylinders)
    {
        $id=explode("/",url()->current());
        $id_value=$id[6];
        Oxygen_Cylinders::destroy($id_value);
        return redirect()->route('users.index')->with('success','Product deleted successfully');
    }

    public function create_cylinder_data(array $data)
    {
      return Oxygen_Cylinders::create([
        'users_name' => $data['name'],
        'users_gender'=>$data['gender'],
        'users_age' => $data['age'],
        'users_aadhar_card_number' =>$data['aadhar_number'],
        'users_identity_proof'=>$data['identity_proof'],
        'users_covid19_status'=>$data['covid_19_status'],
        'users_date_of_covid19_positive'=>$data['date_of_covid_19'],
        'users_address'=>$data['address'],
        'users_state'=>$data['state'],
        'users_city'=>$data['city'],
        'users_phone_number'=>$data['contact'],
        'users_booked_cylinder_status'=>$data['bk_cylinder'],
        'cylinder_options'=>$data['cylinder_options'],

      ]);
    }
}
