<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
Use App\Oxygen_Cylinders;
use App\Http\Middleware\PreventBackHistory;


class AuthController extends Controller
{
    public function __contruct(){
        session_start();
        $this->middleware(PreventBackHistory::class);
    }
    public function index()
    {
       
        return view('login');
    }  
 
    public function register()
    {
        return view('register');
    }
     
    public function postLogin(Request $request)
    {
        request()->validate([
        'email' => 'required',
        'password' => 'required',
        ]);
 
        $credentials = $request->only('email', 'password');
        $request->session()->put('users_data',$request->all());
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('users');
        }
        return redirect()->route("login")->withSuccess('Oppes! You have entered invalid credentials');
    }
 
    public function postRegister(Request $request)
    {  
        request()->validate([
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        ]);
         
        $data = $request->all();
 
        $check = $this->create($data);
       
        return redirect()->route("login")->withSuccess('Great! You have Successfully loggedin');
    }
     
    public function dashboard()
    { 
 
      if(Auth::check()){
        $products = Oxygen_Cylinders::latest()->paginate(5);
  
        return view('users.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
        //return view('dashboard');
      }
       return redirect()->route("login")->withSuccess('Opps! You do not have access');
    }
 
    public function create(array $data)
    {
      return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
    }
     
    public function logout() {
        session()->flush();
        Auth::logout();
        return Redirect('login');
    }

   
}
