<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oxygen_Cylinders extends Model
{
    protected $fillable = [
        'users_name','users_gender','users_age','users_aadhar_card_number','users_identity_proof','users_covid19_status','users_date_of_covid19_positive','users_address','users_state','users_city','users_phone_number','users_booked_cylinder_status','cylinder_options'
    ];
}
