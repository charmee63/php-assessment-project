<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier__users', function (Blueprint $table) {
            $table->id();
            $table->string('supplier_name');
            $table->string('supplier_gender');
            $table->integer('supplier_age');
            $table->string('supplier_aadhar_card_number');
            $table->text('supplier_identity_proof');
            $table->text('supplier_address');
            $table->string('supplier_state');
            $table->string('supplier_city');
            $table->string('supplier_phone_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_users');
    }
}
