<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOxygenCylindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oxygen__cylinders', function (Blueprint $table) {
            $table->id();
            $table->string('users_name');
            $table->string('users_gender');
            $table->integer('users_age');
            $table->string('users_aadhar_card_number');
            $table->text('users_identity_proof');
            $table->string('users_covid19_status');
            $table->string('users_date_of_covid19_positive')->nullable();;
            $table->text('users_address');
            $table->string('users_state');
            $table->string('users_city');
            $table->string('users_phone_number');
            $table->string('users_booked_cylinder_status');
            $table->string('cylinder_options');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oxygen_cylinders');
    }
}
