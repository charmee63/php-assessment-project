@extends('users.layout')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Oxygen Cylinder</h2>
        </div>
        <div class="pull-right">
            {{-- <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a> --}}
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form id="form_1" action="" method="POST"  enctype="multipart/form-data">
    @csrf
  
     <div class="row">
       
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" id="name" class="form-control eventAllForm" placeholder="Name">
                <span class="name_error" id="name_error"></span>
            </div>
        </div>
       
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Age:</strong>
                <input type="number" name="age" id="age" class="form-control eventAllForm" placeholder="Name">
                <span class="age_error" id="age_error"></span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong> Gender:</strong>
               Male:   <input type="radio" name="gender" value="Male" checked>
               Female: <input type="radio" name="gender" value="Female">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Addhar Card Number:</strong>
                <input type="number" name="aadhar_number" id="aadhar_number" class="form-control eventAllForm" placeholder="Name">
                <span class="aadhar_number_error" id="aadhar_number_error"></span>
            </div>
        </div>
       
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Covid 19 Status:</strong>
                <select name="covid_19_status" id="covid_19_status" class="form-control eventAllForm" placeholder="covid 19 startus">
                    <option value="">Select Covid 19 Status</option>
                    <option value="positive">positive</option>
                    <option value="negavtive">negavtive</option>
                </select>
                <span class="covid_19_status_error" id="covid_19_status_error"></span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6" id="covid_date" hidden>
            <div class="form-group">
                <strong>Date of Covid 19:</strong>
                <input type="date" name="date_of_covid_19" id="date_of_covid_19" class="form-control eventAllForm" placeholder="Name">
                <span class="date_of_covid_19_error" id="date_of_covid_19_error"></span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Identity Proof:</strong>
                
                
                <input type="text"  name="identity_proof" id="identity_proof" class="form-control">
                <span class="file_error" id="file_error"></span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Address:</strong>
                <textarea id="address" class="form-control eventAllForm" style="height:150px" name="address" placeholder="Detail"></textarea>
                <span class="address_error" id="address_error"></span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>State:</strong>
                <select name="state" id="state" class="form-control eventAllForm" placeholder="State">
                    <option value="">Select State</option>
                </select>
                <span class="state_error" id="state_error"></span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>City:</strong>
                <select name="city" id="city" class="form-control eventAllForm" placeholder="Name">
                    <option value="">Select City</option>
                </select>
                <span class="city_error" id="city_error"></span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Phone No:</strong>
                <input type="number" name="contact" id="contact" class="form-control eventAllForm" placeholder="Name">
                <span class="contact_error" id="contact_error"></span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Booked  Cylinder:</strong>
                <input type="text" name="bk_cylinder" id="bk_cylinder" class="form-control eventAllForm" placeholder="Name" value="Processing" readonly>
               
                <span class="bk_cylinder_error" id="bk_cylinder_error"></span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Type of Cylinder:</strong>
                <select name="cylinder_options" id="cylinder_options" class="form-control eventAllForm" placeholder="cylinder options">
                    <option value="">Select type of cylinder</option>
                    <option value="5 Ltr">5 Ltr</option>
                    <option value="10 Ltr">10 Ltr</option>
                    <option value="15 Ltr">15 Ltr</option>
                </select>
              
                <span class="cylinder_options_error" id="cylinder_options_error"></span>
            </div>
        </div>
      
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button  id="submit_data" type="button"  class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>

<script>
$(document).ready(function() {
    load_json_data('state');

    function load_json_data(id, parent_id) {
        var html_code = '';
        $.getJSON('../../data_json.json', function(data) {

            html_code += '<option value="">Select ' + id + '</option>';
            $.each(data, function(key, value) {
                if (id == 'state') {
                    if (value.parent_id == '0') {
                        html_code += '<option value="' + value.id + '">' + value.name + '</option>';
                    }
                } else {
                    if (value.parent_id == parent_id) {
                        html_code += '<option value="' + value.id + '">' + value.name + '</option>';
                        console.log(value.parent_id + " " + value.name);
                    }
                }
            });
            $('#' + id).html(html_code);
        });

    }


    $(document).on('change', '#state', function() {
        var country_id = $(this).val();

        if (country_id != '') {
            load_json_data('city', country_id);
        } else {
            $('#city').html('<option value="">Select city</option>');
        }
    });

    $("select[name=covid_19_status]").on("change", function() {
        var status = $(this).val();
        if (status == "positive") {
            $("#covid_date").removeAttr("hidden");

        } else {
            $("#covid_date").attr("hidden", true);
        }
    });



    function validate(type) {
        var validate_status = 1;
        $('.' + type).each(function() {
            var id = $(this).attr('id');
            if ($('#' + id).val() == "") {
                $("#" + id + "_error").text("This field is required");
                $("#" + id + "_error").attr("class", "error");
                $("#" + id + "_error").addClass('is-invalid');
                $("#" + id + "_error").css('color', 'red');
                validate_status = 0;
            } else {
                $("#" + id + "_error").text("");
                $("#" + id + "_error").removeClass('is-invalid');
                $("#" + id + "_error").css('color', '');
                validate_status = 1;
            }
        })
        if (validate_status == 1) {
            return true;
        } else {
            return false;
        }
    }
    $("#submit_data").click(function() {

        var contact = $("#contact").val();
        var aadhar_number = $("#aadhar_number").val();
        var age = $("#age").val();
        var contact_no_regs = /^[789]\d{9}$/;
        var aadhar_card_number = /^[1-9]\d{11}$/;
        if (validate('eventAllForm') == true) {


            if (age.length < 1 || age.length > 2) {
                $("#age_error").text("please enter correct age");
                $("#age_error").attr("class", "error");
                $("#age_error").addClass('is-invalid');
                $("#age_error").css('color', 'red');
            } else if (!aadhar_card_number.test(aadhar_number)) {
                $("#aadhar_number_error").text("please enter correct addhar no");
                $("#aadhar_number_error").attr("class", "error");
                $("#aadhar_number_error").addClass('is-invalid');
                $("#aadhar_number_error").css('color', 'red');
            } else if (!contact_no_regs.test(contact)) {
                $("#contact_error").text("please enter correct contact no");
                $("#contact_error").attr("class", "error");
                $("#contact_error").addClass('is-invalid');
                $("#contact_error").css('color', 'red');

            } else {
                $("#contact_error").text("");
                $("#contact_error").removeClass('is-invalid');
                $("#contact_error").css('color', '');
                $("#aadhar_number_error").text("");
                $("#aadhar_number_error").removeClass('is-invalid');
                $("#aadhar_number_error").css('color', '');
                $("#age_error").text("");
                $("#age_error").removeClass('is-invalid');
                $("#age_error").css('color', '');

                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '<?= route("create_users") ?>',
                    data: $("#form_1").serialize(),
                    beforeSend: function() {
                        // showLoader();
                    },
                    success: function(result) {
                        console.log(result);
                    }
                });


            }



        }
    });


});

</script>

@endsection
