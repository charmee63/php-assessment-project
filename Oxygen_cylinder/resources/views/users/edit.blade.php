@extends('users.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Oxygen Culinder Status</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('users.update',$oxygen_cylinders->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Oxygen cylinder status:</strong>
                    <?php $status=$oxygen_cylinders->users_booked_cylinder_status; ?>
                     <select name="users_booked_cylinder_status"  class="form-control" placeholder="Name">
                    <option value="Processing" <?php ($status=="Processing")?"selected":""; ?>>Processing</option>
                    <option value="Delivered" <?php ($status=="Delivered")?"selected":""; ?>>Delivered</option>
                    <option value="Cancelled" <?php ($status=="Cancelled")?"selected":""; ?>>Cancelled</option>
                </select>
                   
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection