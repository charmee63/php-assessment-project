@extends('users.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Oxygen Cylinders </h2>
            </div>
            <div class="pull-right">
               
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    
    <table  id='empTable' width='100%' border="1" style='border-collapse: collapse;'>
        <thead>
            <tr>
              <td>S.no</td>
              <td>Name</td>
              <td>State</td>
              <td>City</td>
              <th width="280px">Action</th>
            </tr>
          </thead>
       
    </table>
   
    <script type="text/javascript">
        $(document).ready(function(){
    
          // DataTable
          $('#empTable').DataTable({
             processing: true,
             serverSide: true,
             ajax: "{{route('getEmployees')}}",
             columns: [
                { data: 'id' },
                { data: 'username' },
                { data: 'name' },
                { data: 'email' },
                {data:'actions'}
             ]
          });
    
        });
        </script>
    
@endsection