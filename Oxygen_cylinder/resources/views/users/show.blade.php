@extends('users.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Oxygen Cylinder</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $oxygen_cylinders->users_name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Age:</strong>
                {{ $oxygen_cylinders->users_age }}
            </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong> Gender:</strong>
                {{ $oxygen_cylinders->users_gender }}
             
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Addhar Card Number:</strong>
                {{ $oxygen_cylinders->users_aadhar_card_number }}
            </div>
        </div>
       
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Covid 19 Status:</strong>
                {{ $oxygen_cylinders->users_covid19_status }}
              
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Date of Covid 19:</strong>
                {{ $oxygen_cylinders->users_date_of_covid19_positive }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Identity Proof:</strong>
                {{ $oxygen_cylinders->users_identity_proof }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Address:</strong>
                {{ $oxygen_cylinders->users_address }}
              
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>State:</strong>
                {{ $oxygen_cylinders->users_state }}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>City:</strong>
                {{ $oxygen_cylinders->users_city }}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Phone No:</strong>
                {{ $oxygen_cylinders->users_phone_number }}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Booked  Cylinder:</strong>
                {{ $oxygen_cylinders->users_booked_cylinder_status }}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Type of Cylinder:</strong>
                {{ $oxygen_cylinders->cylinder_options }}
               
            </div>
        </div>
    </div>
@endsection