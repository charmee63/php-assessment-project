<!DOCTYPE html>
<html>
<head>
    <title>Oxygen Cylinder</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    
     <!-- CSS -->
    <!-- Datatables CSS CDN -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

    <!-- jQuery CDN -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 

    <!-- Datatables JS CDN -->
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> 
    
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">Oxygen Cylinder</a>
          </div>
          <?php $dsiplay=""; ?>
          <ul class="nav navbar-nav">
            @if (Route::currentRouteName()=="users.show" || Route::currentRouteName()=="users.index" || Route::currentRouteName()=="users.edit") 
              <?php $dsiplay="none";?>
            @endif
            <li><a href="<?= route("users.create")?>" style="display:{{$dsiplay}}">Book Cylinder</a></li>
            <li><a href="<?= route("register")?>" style="display:{{$dsiplay}}">Register (Supplier)</a></li>
            <li><a href="<?= route("login")?>" style="display:{{$dsiplay}}">Login (Supplier)</a></li>
          
            @if(Route::currentRouteName()=="users.show" || Route::currentRouteName()=="users.index" || Route::currentRouteName()=="users.edit")
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                  <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
              </div>
          </li>
          @endif
       
          </ul>
        
        </div>
      </nav>
<div class="container">
    @yield('content')
</div>
   
</body>
</html>